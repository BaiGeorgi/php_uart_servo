#include <Servo.h> 
Servo myservo1;
Servo myservo2;
Servo camera;
int count=90;
void setup() {
  Serial.begin(9600);
  myservo1.detach();
  myservo2.detach();
  camera.attach(11);
  camera.write(count);
  Serial.println("Ready");
}

void loop() {
  if (Serial.available() > 0) {
    int inByte = Serial.read();
    switch (inByte) {
     case 'w'://Move forward
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(0);
      delay(10);
     break;
     case 's'://Move backward
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(0);
      myservo2.write(180);
      delay(10);
     break;
     case 'a'://Rotate left
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(0);
      myservo2.write(0);
      delay(10);
     break;
     case 'd'://Rotate right
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(180);
      delay(10);
     break;
     case 'q'://Rotate left
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(0);
      myservo2.write(0);
      delay(50);
      myservo1.detach();
      myservo2.detach();
     break;
     case 'e'://Rotate right
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(180);
      delay(50);
      myservo1.detach();
      myservo2.detach();
     break;
     case '1'://Shake
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.detach();
      myservo2.detach();
     break;
     case '0'://Stop
      myservo1.detach();
      myservo2.detach();
     break;
     case 'z'://Stop
      myservo1.detach();
      myservo2.detach();
     break;
     case 'r'://Adjust camera position up with 1°
      count++;
      Serial.println(count);
      camera.attach(11);
      camera.write(count);
     break;
     case 'f'://Adjust camera position down with 1°
      count--;
      Serial.println(count);
      camera.attach(11);
      camera.write(count);
     break;
    } 
  }
}