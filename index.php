<?php
function openSerial($command) {
	$openSerialOK = false;
	try {
		exec("stty /dev/ttyAMA0: BAUD=9600 PARITY=n DATA=8 STOP=1 to=off dtr=off rts=off");
		//$fp =fopen("com4", "w");
		$fp = fopen('/dev/ttyAMA0','r+');
		$openSerialOK = true;
	} catch(Exception $e) {
		echo 'Message: ' .$e->getMessage();
	}

	if($openSerialOK) {
		fwrite($fp, $command);
		fclose($fp);
    }	
}

openSerial();

if(isset($_POST['fw'])) {
    openSerial("w\r");
    usleep(500000);
    openSerial("0\r");
}

if(isset($_POST['bw'])) {
    openSerial("s\r");
    usleep(500000);
    openSerial("0\r");
}

if(isset($_POST['l'])) {
    openSerial("l\r");
    usleep(50000);
}

if(isset($_POST['r'])) {
    openSerial("r\r");
    usleep(50000);
}

if(isset($_POST['stop'])) {
    openSerial("0\r");
}
?>
<html>
<head><meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"><title>Версия 2</title><body bgcolor="#000000"></head>
<table>
<tr>
<td><img src="http://pi:8081" /><br></td>
 <td>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
   <input type="submit" name="fw" value="Напред"><br>
   <input type="submit" name="bw" value="Назад"><br>
   <input type="submit" name="l" value="Ляво"><br>
   <input type="submit" name="r" value="Дясно"><br>
<!--   <input type="submit" name="stop" value="Стоп"><br>-->
</form>
</td>
</tr>
</html>
