Control more than one servo motor over UART with Raspberry Pi or Olinuxino with Arduino Duemilanove compatable board. I've designed the PCB, so it can be single layer and easy for toner transfer. If you want to use [ceramic resonator](http://en.wikipedia.org/wiki/Ceramic_resonator) you don't need the 22pf capacitors!

**To power the Atmega, you have to conect GND and 3V3 pins from Raspberry Pi/Olinuxino to the UART header.**


[![Demonstration](http://img.youtube.com/vi/vqgdQZZN4Xo/0.jpg)](http://youtu.be/vqgdQZZN4Xo)